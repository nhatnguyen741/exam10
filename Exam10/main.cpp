//
//  main.cpp
//  Exam10
//
//  Created by Nhat Nguyen on 21/02/2023.
//

#include <iostream>
using namespace std;

// Class node, include data and one pointer,
// point at the next pointer node.
class Node {
public:
    int data;
    Node *next;
    
    // Default constructor
    Node() {
        data = 0;
        next = NULL;
    }
    
    // Parametered Constructor
    Node (int data){
        this->data = data;
        this->next = NULL;
    }
};

// Linked list class to
// implement a linked list
class LinkedList {
    Node *head;
    
public:
    // Default constructor
    LinkedList() {
        head = NULL;
    }
    
    // function to insert a node
    // at the end of the
    // linked list
    void insertNode(int);
    
    // function to print the
    // linked list
    void printList();
    
    // Function to delete the node
    // at give position
    void deleteNode(int);
};

// Function to insert a new node
void LinkedList::insertNode(int data) {
    // Create the new Node
    Node *newNode = new Node(data);
    
    // Assign to head
    if(head == NULL) {
        head = newNode;
        return;
    }
    
    // Traverse to the end of list
    // Step 1: assign temp = head
    // Step 2: temp go to -> last one
    // Step 3: last one's next pointer = new node
    Node *temp = head;
    while(temp->next != NULL) {
        // Update temp
        temp = temp->next;
    }
    
    // Insert at the last
    temp->next = newNode;
}

// Function to delete the
// node at given position
void LinkedList::deleteNode(int nodeIndex){
    Node *temp1 = head, *temp2 = NULL;
    int listLength = 0;
    
    if (head == NULL) {
        cout << "List empty." << endl;
        return;
    }
    
    // Find length of the linked list
    while (temp1 != NULL){
        temp1 = temp1->next;
        listLength++;
    }
    
    // Check if the position to be
    // deleted is greater than the length
    // of the linked list
    if (listLength < nodeIndex) {
        cout << "Index out of range." << endl;
        return;
    }
    
    // Declare temp1
    temp1 = head;
    
    // Deleting the head
    if (nodeIndex == 1) {
        // Update head
        head = head->next;
        delete temp1;
        return;
    }
    
    // Traverse the list to
    // find the node to be deleted
    // index of linked list start from 1, because the size of list
    // is controled directly, and index is not tell the distance of
    // element
    while (nodeIndex-- > 1) {
        // Update temp2, move index of temp2 = temp1
        temp2 = temp1;
        
        // Update temp1, move index of temp1 to next index
        temp1 = temp1->next;
    }
    
    // Change the next pointer
    // of the previous node.
    temp2->next = temp1->next;
    
    // Delete the node
    delete temp1;
}

// Function to print the
// nodes of the linked list
void LinkedList::printList(){
    Node *temp = head;
    
    // Check for empty list
    if (head == NULL) {
        cout << "List empty." << endl;
        return;
    }
    
    // Traverse the list
    while (temp != NULL) {
        cout << temp->data << " ";
        temp = temp->next;
    }
}

int main(int argc, const char * argv[]) {
    LinkedList list;
    
    // inserting node
    for (int i = 1; i <= 9; i++) {
        list.insertNode(i);
    }
    
    cout << "Elements of the list are: ";
    // Print the list
    list.printList();
    cout << endl;
    
    // Delete node at position 2.
    list.deleteNode(2);
    
    cout << "Elements of the list are: ";
    list.printList();
    cout << endl;
    return 0;
}
